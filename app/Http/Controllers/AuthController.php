<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('register');
    }
    public function kirim(Request $request){
        $first = $request['depan'];
        $last = $request['belakang'];
        return view('welcome', compact('first','last'));
    }
}
